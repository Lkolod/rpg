
#ifndef RPG_PROFESSION_H
#define RPG_PROFESSION_H
#include "character.h"
#include "hero.h"


class Mage :public hero{
public:
    static void increase_stats(character& obj);
};

class warrior :public hero{
public:
    static void increase_stats(character& obj);
};

class berserker: public hero{
public:
    static void increase_stats(character& obj);
};
class thief: public hero{
public:
    static void increase_stats(character& obj);
};

#endif //RPG_PROFESSION_H
