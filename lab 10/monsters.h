#ifndef RPG_MONSTERS_H
#define RPG_MONSTERS_H
#include "character.h"
#include <string>
#include <fstream>

using namespace std;

class monsters: public hero {
    public:
        monsters();
        void dispStats();
        int get_s();
        int get_d();
        int get_e();
        int get_i();
        int get_c();
};


#endif //RPG_MONSTERS_H
