#include <iostream>
#include <utility>
#include "character.h"
#include "profession.h"
#include "monsters.h"

using namespace std;

character create_new(){
// to do change the lines counter
    fstream file;
    string text;

    string temp_name;
    int temp_strength;
    int temp_dexterity;
    int temp_endurance;
    int temp_intelligence;
    int temp_charisma;

    cout<<"Set name of character: ";
    cin>>temp_name;
    cout<<"Set strength of character: ";
    cin>>temp_strength;
    cout<<"Set dexterity of character: ";
    cin>>temp_dexterity;
    cout<<"Set endurance of character: ";
    cin>>temp_endurance;
    cout<<"Set intelligence of character: ";
    cin>>temp_intelligence;
    cout<<"Set charisma of character: ";
    cin>>temp_charisma;

    return {temp_name,temp_strength,temp_dexterity, temp_endurance, temp_intelligence, temp_charisma};
}

character load_character(string name){
    return {std::move(name)};

}
void select_profession(){
    cout << "select profession of your character";
    // TO DO
}
void save_monsters(monsters* MONSTERS){
    fstream file;
    file.open("monsters.txt", ios::out);
    if(file.good()) {
        for(int i =0; i<5; i++ ){
            file<<"Monster "<<i+1<<endl;
            file<<MONSTERS[i].get_s()<<endl;
            file<<MONSTERS[i].get_d()<<endl;
            file<<MONSTERS[i].get_e()<<endl;
            file<<MONSTERS[i].get_i()<<endl;
            file<<MONSTERS[i].get_c()<<endl;
        }
    }
}

int main() {
    int choice = 0;
    string character_choice;
    auto* MONSTER = new monsters[5];

    do {
        cout << "LOAD CHARACTER PRESS: 1 \t" << "CREATE NEW CHARACTER PRESS: 2" << endl;
        cin >> choice;
    }
    while (choice < 0 || choice > 2);

    if(choice == 1){
        cout << "write name of your character" << endl;
        cin >> character_choice;
        character CHARACTER = load_character(character_choice);
        cout << "you have chosen: " << endl;
        CHARACTER.dispStats();
        CHARACTER.save();
    }
    else if(choice == 2){
        character CHARACTER = create_new();
        CHARACTER.dispStats();
        CHARACTER.save();
    }
    for(int i=0;i<5;i++) {
        MONSTER[i] = monsters();
        cout<<"Monster "<<i+1 <<endl;
        MONSTER[i].dispStats();
        cout<<"\n";
    }
    save_monsters(MONSTER);

    return 0;
}
