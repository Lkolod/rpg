
#ifndef RPG_PROFESSION_H
#define RPG_PROFESSION_H
#include "character.h"


class hero {
public:
    virtual void increase_stats() = 0;
};

class Mage :public hero{
public:
    static void increase_stats(character& obj);
};

class warrior :public hero{
public:
    static void increase_stats(character& obj);
};

class berserker: public hero{
public:
    static void increase_stats(character& obj);
};
class thief: public hero{
public:
    static void increase_stats(character& obj);
};

#endif //RPG_PROFESSION_H
