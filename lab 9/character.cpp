#include "character.h"

#include <utility>

using namespace std;


character::character() = default;

character::character(string newname, int newstrength, int newdexterity, int newendutance, int newintelligence, int newcharisma) {
    name = std::move(newname);
    strength = newstrength;
    dexterity = newdexterity;
    endurance = newendutance;
    intelligence = newintelligence;
    charisma = newcharisma;
}

character::character(string character_file) {

    fstream file;
    name = std::move(character_file);
    file.open(name +".txt", ios::in);
    if (file.good()) {
        file >> name >>  strength >> dexterity >> endurance >> intelligence >> charisma;
    }
    else {
        cout << "wrong file";
    }
    file.close();

}
// add condition for id later
character::~character() = default;

void character::save(){
    fstream file;

    file.open(name+".txt", ios::out);
    if (file.good()) {
        file << name << " " << strength << " " << dexterity << " " << endurance << " " << intelligence << " "
             << charisma << " ";
    }
    else {
        cout << "wrong file";
    }
    file.close();
}

void character::dispStats() {
    cout<<"Statistics of: "<<name<<endl;
    cout<<"Strength = "<<strength<<endl;
    cout<<"Dexterity = "<<dexterity<<endl;
    cout<<"Endurance = "<<endurance<<endl;
    cout<<"Intelligence = "<<intelligence<<endl;
    cout<<"Charisma = "<<charisma<<endl;
    cout << endl;
}

int character::get_s(){
    return strength;
}

int character::get_d(){
    return dexterity;
}
int character::get_e(){
    return endurance;
}
int character::get_i(){
    return intelligence;
}
int character::get_c(){
    return charisma;
}

