#ifndef RPG_CHARACTER_H
#define RPG_CHARACTER_H

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class character {
private:

    string name;
    int strength{};
    int dexterity{};
    int endurance{};
    int intelligence{};
    int charisma{};

// zapytac o to
public:
    friend class Mage;
    friend class warrior;
    friend class berserker;
    friend class thief;
    friend class monsters;

    character();
    character(string,int,int,int,int,int);
    character(string);
    ~character();
    void save();
    void dispStats();
    int get_s();
    int get_d();
    int get_e();
    int get_i();
    int get_c();
};


#endif //RPG_CHARACTER_H
