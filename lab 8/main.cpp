#include <iostream>
#include <utility>
#include "character.h"

using namespace std;

character create_new(){
// to do change the lines counter
    fstream file;
    string text;

    string temp_name;
    int temp_strength;
    int temp_dexterity;
    int temp_endurance;
    int temp_intelligence;
    int temp_charisma;

    cout<<"Set name of character: ";
    cin>>temp_name;
    cout<<"Set strength of character: ";
    cin>>temp_strength;
    cout<<"Set dexterity of character: ";
    cin>>temp_dexterity;
    cout<<"Set endurance of character: ";
    cin>>temp_endurance;
    cout<<"Set intelligence of character: ";
    cin>>temp_intelligence;
    cout<<"Set charisma of character: ";
    cin>>temp_charisma;

    return {temp_name,temp_strength,temp_dexterity, temp_endurance, temp_intelligence, temp_charisma};
}

character load_character(string name){
    return {std::move(name)};

}

int main() {
    int choice = 0;
    string character_choise;
    do {
        cout << "LOAD CHARACTER PRESS: 1 \t" << "CREATE NEW CHARACTER PRESS: 2" << endl;
        cin >> choice;
    }
    while (choice < 0 || choice > 2);

        if(choice == 1){
            cout << "write name of your character" << endl;
            cin >> character_choise;
            character CHARACTER = load_character(character_choise);
            cout << "you have chosen: " << endl;
            CHARACTER.dispStats();
            CHARACTER.save();
        }
        else if(choice == 2){
            character CHARACTER = create_new();
            CHARACTER.dispStats();
            CHARACTER.save();

        }

    return 0;
}
