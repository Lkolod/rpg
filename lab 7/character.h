#ifndef RPG_CHARACTER_H
#define RPG_CHARACTER_H

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class character {
    private:

        string name;
        int strength{};
        int dexterity{};
        int endurance{};
        int intelligence{};
        int charisma{};

// zapytac o to
    public:
        character(string,int,int,int,int,int);
        character(string);
        ~character();
        void save();
        void dispStats();


};


#endif //RPG_CHARACTER_H
